<?php $this->view('layout/body_header') ?>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
            <?php $this->view('layout/menu_profile')?>
            <!-- /menu profile quick info -->
            <br />
            <!-- sidebar menu -->
           <?php $this->view('layout/sidebar')?>
            <!-- /sidebar menu -->
            <!-- /menu footer buttons -->
            <?php $this->view('layout/menu_footer')?>
            <!-- /menu footer buttons -->
          </div>
        </div>
        <!-- top navigation -->
        <?php $this->view('layout/top_nav')?>
        <!-- /top navigation -->
        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
           
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>COIN CONVERTED</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                          <li><a href="#">Settings 1</a>
                          </li>
                          <li><a href="#">Settings 2</a>
                          </li>
                        </ul>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <p class="text-muted font-13 m-b-30">
                      
                  </p>
                  <div class="x_content">
                    <p class="text-muted font-13 m-b-30">   
                  
                    </p>

                    <table id="datatable-button" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                            <th>#</th>
                            <th>Transaction No</th>
                            <th>Username</th>
                            <th>Date</th>
                            <th>Amount (In USD)</th>
                            <th>Coins Receive</th>
                            <th>Payment Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $i= 0 ;  foreach($withdrawals as $withdrawals) { $i +=1;?>
                        <tr>
                            <td><?=$i?></td>
                            <td><?=$withdrawals['user_id']?></td>
                            <td><?=$withdrawals['username']?></td>
                            <td><?=$withdrawals['date']?></td>
                            <td><?=$withdrawals['amount']?></td>
                            <td><?=$withdrawals['coins']?></td>
                            <td>Paid</td>
                        </tr> 
                        <?php } ?> 
                        
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          
          </div>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <?php $this->view('layout/footer') ?>
        <!-- /footer content -->
      </div>
    </div>
    <?php $this->view('layout/scripts') ?>

     <script src="<?=base_url()?>assets/admin/vendors/iCheck/icheck.min.js"></script>
    <!-- Datatables -->
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/jszip/dist/jszip.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pdfmake/build/vfs_fonts.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
     <script src="<?=base_url()?>assets/admin/vendors/pnotify/dist/pnotify.js"></script>
    <script src="<?=base_url()?>assets/admin/vendors/pnotify/dist/pnotify.buttons.js"></script>

     <script src="<?=base_url()?>assets/js/jquery-confirm.js"></script>

    <script>
       $(document).ready(function () {

                $('[name=search]').submit(function(){
                     $('#btn_submit').html('Searching downlines   <i class="fa fa-spinner"></i>');
                     $('#btn_submit').attr('disabled',true);

                });
               $('.ui-pnotify').remove();
               $('#datatable-button').DataTable( {dom: "Bfrtip",
                            buttons: [{
                                extend: "copy",
                                className: "btn-success"
                            }, {
                                extend: "csv"
                            }, {
                                extend: "excel"
                            }, {
                                extend: "pdf"
                            }, {
                                extend: "print"
                            }],
                            "language": {
                                     "loadingRecords": "<span class='fa fa-spin fa-spinner'> </span> Please wait - retrieving data..."
                                  },
                            responsive: !0 });
        });
    </script>
    <script type="text/javascript">
      
        $("body").on("click", "#datatable-button  td a.del_button", function(e) {
            e.returnValue = false;
            var transaction_no = this.id; 
            var userid = $(this).attr('data-user'); 
            var amount = $(this).attr('data-amount');

            $(this).html('<span>Processing <i class="fa fa-spinner"></i></span>');
            $(this).attr('disabled',true);

            //console.log(myData); 

            var $tr = $(this).closest('tr'); //here we hold a reference to the clicked tr which will be later used to delete the row

                    console.log($(this).attr('data-user'));
                    console.log($(this).attr('data-amount'));
                        $.ajax({
                            type: "POST", // HTTP method POST or GET
                            url: "pay", //Where to make Ajax calls
                            data:{ 
                                  transaction_no : transaction_no ,
                                  user_id        : userid,
                                  amount         : amount
                                  }, //Form variables
                            success:function(response){
                                //on success, hide  element user wants to delete.
                                var obj = JSON.parse(response);

                                console.log(obj.title);
                
                                new PNotify({
                                            title: obj.title,
                                            text: obj.text,
                                            type: obj.type,
                                            styling: 'bootstrap3'
                                          });
                                if(obj.type =="success") {
                                     $tr.find('td').fadeOut(1000,function(){ 
                                    $tr.remove();                    
                                   }); 
                                }
                               
                                 $(this).html('<span>Mark as Paid</span>');
                                $(this).attr('disabled',false);


                            },
                            error:function (xhr, ajaxOptions, thrownError){
                                //On error, we alert user
                                alert(thrownError);


                            }
                        });
               

});
    </script>

    <!-- <script type="text/javascript" src="<?=base_url()?>assets/admin/js/withdrawal.js">   </script> -->

<?php $this->view('layout/body_footer')?>