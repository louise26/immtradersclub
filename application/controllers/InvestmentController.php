<?php
defined('BASEPATH') or exit('No direct script access allowed');



class InvestmentController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');	

	}

	public function index () {
			
				
				if($this->is_logged_in() ) {

					$userid = " ";

						foreach ($this->model_users->select('userid',['user_id' => $this->auth_user_id]) as $key => $value) {
								$userid = $value->userid;
						}
						$data = [
									'userid' => $userid
								];		
					return $this->load->view('investmentpackage/investmentpackage',$data);
				}
				else {
					redirect('login');
				}
	}

}