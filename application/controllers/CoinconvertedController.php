<?php
defined('BASEPATH') or exit('No direct script access allowed');



class CoinconvertedController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		// Models loaded
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_add_fund_request');
		$this->load->model('model_credit_amt');
		$this->load->model('model_withdraw_request');

	}

	public function index () {
			
				if($this->is_logged_in() ) {	

						$result = [] ;
						foreach ($this->model_lifejacket_subscription->query("SELECT lifejacket_subscription_coin_converted.*,user_registration.username FROM lifejacket_subscription_coin_converted  JOIN  user_registration ON lifejacket_subscription_coin_converted.user_id=user_registration.user_id order  by lifejacket_subscription_coin_converted.id desc")->result() as $key => $value) {

								array_push($result,[
														'user_id'	=> $value->user_id,
														'username'	=> $value->username,
														'amount' 	=> $value->amount,
														'date'		=> $value->date,
														'coins'		=> $value->pb,
			
													]);
						}

						$data = [
									'withdrawals' => $result,
									'datefrom'	  => 'No date set',
									'dateto'	  => 'No date set',	
									'userid'	 => '<i>(NO USER SELECTED)</i>',
									'ttype'		 => '<i>Search Transaction</i>',
									'total'			=> 0

								] ;
					return $this->load->view('reports/coinconverted',$data);
				}
				else {

					redirect('login');
				}
	}
}