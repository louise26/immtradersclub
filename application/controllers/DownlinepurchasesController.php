<?php
defined('BASEPATH') or exit('No direct script access allowed');



class DownlinepurchasesController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		// Models loaded
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_add_fund_request');
		$this->load->model('model_credit_amt');
		$this->load->model('model_withdraw_request');

	}

	public function index () {

				if($this->is_logged_in() ) {







						$data = [
									'withdrawals' => [],
									'datefrom'	  => 'No date set',
									'dateto'	  => 'No date set',
									'userid'	 => '<i>(NO USER SELECTED)</i>',
									'ttype'		 => '<i>Search Transaction</i>',
									'total'			=> 0

								] ;
					return $this->load->view('reports/downlinepurchases',$data);
				}
				else {

					redirect('login');
				}
	}

	public function searchIncome(){

		if( $this->is_logged_in() ){

					$userid 	= $this->input->post('userid');
					$df 			= $this->input->post('df');
					$dt 			= $this->input->post('dt');
					$tt 			= $this->input->post('tt');
					$result 	= [] ;
					$condition	= "";
					$total 		= 0 ;


					if($df!='' && $df=='')
				    {
				        $condition=" and (date='$df')";
				    }
				    if($df=='' && $df!='')
				    {
				        $condition=" and (date = '$df')";
				    }
				    if($df!='' && $df!='')
				    {
				        $condition=" and (date between '$df' and '$df')";
				    }

					foreach ($this->model_credit_amt->query("SELECT * FROM matrix_downline_ref  WHERE income_id='".$userid."' order by level ASC")->result() as $key => $value) {

							foreach ($this->model_users->query("SELECT *,user_registration.username,user_registration.user_id as usr FROM lifejacket_subscription  JOIN user_registration ON lifejacket_subscription.user_id=user_registration.user_id WHERE lifejacket_subscription.user_id='".$value->down_id."' AND lifejacket_subscription.remark='Package Purchase' $condition")->result() as $key => $value1) {

									array_push($result,[
																			'user_id'	=> $value->down_id,
																			'username'	=> $value1->username,
																			'amount' 	=> $value1->amount,
																			'level'		=> $value->level,
																			'remark'	=> 'Package Purchase',
																			'date'		=> $value1->date,
																		]);

									$total = $total + $value1->amount;
								}

					}

						$data = [
											'withdrawals' => $result,
											'datefrom'	  => $dt,
											'dateto'	  => $df,
											'userid'	 => $userid,
											'ttype'		 => $tt ,
											'total'			=> $total
								];


				return $this->load->view('reports/downlinepurchases',$data);
		}
		else {
			redirect('login');
		}
	}

}
