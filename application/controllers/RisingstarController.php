<?php
defined('BASEPATH') or exit('No direct script access allowed');



class RisingstarController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		// Models loaded
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_add_fund_request');
		$this->load->model('model_credit_amt');
		$this->load->model('model_withdraw_request');

	}

	public function index () {

				if($this->is_logged_in() ) {

						$data = [
									'withdrawals' => [],
									'datefrom'	  => 'No date set',
									'dateto'	  => 'No date set',
									'userid'	 => '<i>(NO USER SELECTED)</i>',
									'ttype'		 => '<i>Search Transaction</i>',
								] ;

								return $this->load->view('royalty/risingstar',$data);
				}
				else {

					redirect('login');
				}

	}
	public function searchQualifier() {

			if($this->is_logged_in() ) {

						$start1 = date('Y-m-')."01";
						$end1 = date('Y-m-')."31";

						$start = $this->input->post('df');
						$end = $this->input->post('dt');
						$result = [] ;
						foreach ($this->model_users->query("Select user_id,move_rank,qualify_date from rank_achiever where (qualify_date<='$end') and move_rank='Rising Star'")->result() as $key => $value) {

							$c = 0;
							$useid = $value->user_id;
							if($value->move_rank=='Rising Star') {

									$user 	  = $value->user_id;
									$rank 	  = $value->move_rank;
									$mostart  = $value->qualify_date;
									$moend 	  = date('Y-m-d',strtotime($mostart. '+ 30 days'));
									$c 		  = $this->rising_star($user,$start,$end,$mostart,$moend,$rank) ;
							}
							else {
									$c = 0 ;
							}

							if($c > 0) {

							$newdss = $this->model_users->query("Select user_id from rank_achiever where user_id='".$value->user_id."' and (move_rank='Flying Star' OR move_rank='Champion' OR move_rank='Elite' || move_rank='Co-Founder' )")->result();


									if(count($newdss) == 0) {

										foreach ($this->model_users->select('username,first_name,last_name',['user_id'=>$useid]) as $key => $value1) {

											array_push($result, [
																	'user_id'  	=> $value->user_id,
																	'username' 	=> $value1->username,
																	'fullname' 	=> $value1->first_name . " ". $value1->last_name,
																	'rank'		=> $value->move_rank,
																	'date'		=> $value->qualify_date,
																	'action'	=> "<button class='btn btn-info btn-sm' data-user='".$value->user_id."'>Add Bonus</button>"

															]);
										}

									}
							}
						}

						$data = [
									'withdrawals' => $result,
									'datefrom'	  => $start,
									'dateto'	  => $end,
									'userid'	 => '',
									'ttype'		 => '',
								] ;
					return $this->load->view('royalty/risingstar',$data);
				}
				else {
						redirect('login');
				}
		}


	public function rising_star($user,$start,$end,$mostart,$moend,$rank)
		{

		     $k 			=	0;
             $totsum1 		=	0;
             $xscx1 		=	0;
             $xscx 			=	0;
             $common_total 	=	0;
             $twenty_per 	=	0;
             $newmax 		=	0;
             $sideleg 		=	0;
             $twenty_per 	= 1000 * (20/100);
             $amts 			= 1000;

            foreach ($this->model_users->select('user_id',['ref_id'=>$user]) as $key => $value) {

                    $totsum = 0 ;
                    $ref 	= $value->user_id;

                   	foreach ($this->model_users->query("select sum(amount) as newsum from lifejacket_subscription where user_id='$ref' and (date between '$start' and '$end')")->result() as $key => $value1) {
                                    $totsum = $value1->newsum;
                           }
                     $totsumkam 	=	$totsum;
                     $totsum1 		=	$totsum1+$totsum;
                     $totsum_down1 	=	0;

                    foreach ($this->model_users->query("select down_id from matrix_downline_ref where income_id='".$ref."'")->result() as $key => $value2) {

                                $totsum_down = 0;
                                $new_down 	 = $value2->down_id;

                                $totsum_downkam1 = 0 ;

                                 foreach ($this->model_users->query("select sum(amount) as newsumwer from lifejacket_subscription where user_id='$new_down' and (date between '$start' and '$end')")->result() as $key => $value3) {
                                     			 		$totsum_down 	 = 	$value3->newsumwer;
		                                                $totsum_down1 	 = 	$totsum_down1+$totsum_down;
		                                                $totsum_downkam  =	$value3->newsumwer;
		                                                $totsum_downkam1 =	$totsum_downkam1+$totsum_downkam;
                                }
                     }
                     $xscx 	= 	$totsum+$totsum_down1;
		             $xscx1 = 	$xscx1+$xscx;
		             if($xscx>=$twenty_per){
		                 $k++;
		                 $array[] = $xscx;
		                 $newmax=max($array);
					}

         }

          $common_totalWE 		= $xscx1;
          $common_total 		= $xscx1;
          $common_total 		= $common_total-$newmax;
          $restmax 				= $newmax-$twenty_per;
          $amts 				= $amts-$twenty_per;
          $other_leg_demand 	= $amts*25/100;
          $sideleg=$common_total;
          $common_total=$common_totalWE;

           if($common_total>=1000 && $k>0 && $twenty_per>0 && $sideleg>=$other_leg_demand && $newmax>=$twenty_per){

                            return 1;

             }
           else {
                    return 0;
                 }

	}

}
