<?php
defined('BASEPATH') or exit('No direct script access allowed');



class ProfitDistributionController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_profit_share_all');
		$this->load->model('model_profit_share_exclude');
		

	}

	public function index () {


		if($this->is_logged_in() ) {

						$users = [] ;


				foreach ($this->model_users->select('user_id') as $key => $value) {
							
										array_push($users,[
															'user_id' => $value->user_id ,
															'date'	  => date('Y-m-d')

													]);

							}
							if($this->model_profit_share_all->count_ref( [ 'date' => date('Y-m-d')  ]) > 0) {

							}
							else {

									$this->insertCredit1($users);
							}

						$data = [
									'withdrawals' => [],
									'datefrom'	  => 'No date set',
									'dateto'	  => 'No date set',	
									'userid'	 => '<i>(NO USER SELECTED)</i>',
									'ttype'		 => '<i>Search Transaction</i>',
								] ;
	
						return $this->load->view('member_management/profitdistribution',$data);
				}

				else {

						redirect('login');
				}


       
	}
public function insertCredit1($data = []){


				$this->db->insert_batch('profit_share_all', $data); 

			}
	
	public function removeFromList() {

			$result = [] ;

			$userid = $this->input->post('user_id');
			$date   = date('Y-m-d');

			
			if ($this->model_profit_share_exclude->count_ref(['user_id' => $userid ,'date' => $date ] ) <= 0) {

				if( $this->model_profit_share_exclude->insert(['user_id' => $userid,'date' => $date]) ) {

					array_push($result , [
											'title' 	=>'Add Group',
											'text'		=> 'Added to group Irregular',
											'type'		=>'success'
										]);

					$this->model_profit_share_all->delete(['user_id'=>$userid]);
				}
				else {

						array_push($result , [
											'title' 	=>'Add Failed',
											'text'		=> 'Something went wrong',
									 	 	'type'		=>'error'
										]);
				}
			}
			else {
						array_push($result , [
											'title' 	=>'Add Group',
											'text'		=> 'Added to group Irregular',
											'type'		=>'success'
										]);

			}

			
			echo json_encode($result);
	}
	public function removeFromList1() {

			$result = [] ;

			$userid = $this->input->post('user_id');
			$date = date('Y-m-d');

			
			if ($this->model_profit_share_all->count_ref(['user_id' => $userid ,'date' => $date ] ) <= 0) {

				if( $this->model_profit_share_all->insert(['user_id' => $userid,'date' => $date]) ) {

					array_push($result , [
											'title' 	=>'Add Group',
											'text'		=> 'Added to regular group',
											'type'		=>'success'
										]);

					$this->model_profit_share_exclude->delete(['user_id'=>$userid]);

				}
				else {

						array_push($result , [
											'title' 	=>'Add Failed',
											'text'		=> 'Something went wrong',
									 	 	'type'		=>'error'
										]);
				}
				
			}

			
			echo json_encode($result);
	}




	public function getGroup1(){


			$data 		 = array();
			$status 	 = " " ;
			$i 			 = 0 ;
			$user_status = " " ;

			if( $this->is_logged_in() ){

				foreach ($this->model_users->query("Select profit_share_all.user_id,user_registration.first_name,user_registration.last_name,user_registration.username from profit_share_all JOIN user_registration ON profit_share_all.user_id=user_registration.user_id  WHERE profit_share_all.date='".date('Y-m-d')."'  ")->result() as $key => $value) {

							$row 	= array();
							$i 		+= 1;

						array_push($data,[
											$i,
											$value->user_id, 
											$value->username,
											
											"<button class='btn btn-info btn-xs' id='add1' data-id='".$value->user_id."'>Add to Irregular</button>"
									]);	
				}

			}
			else {

					$data[] = [''] ;

			}

		$output = array(
							"data" => $data,
						);  

	 	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}



	public function getGroup2(){


			$data 		 = array();
			$status 	 = " " ;
			$i 			 = 0 ;
			$user_status = " " ;

			if( $this->is_logged_in() ){

				foreach ($this->model_users->query("Select profit_share_exclude.user_id,user_registration.first_name,user_registration.last_name,user_registration.username from profit_share_exclude JOIN user_registration ON profit_share_exclude.user_id=user_registration.user_id  WHERE profit_share_exclude.date='".date('Y-m-d')."'  ")->result() as $key => $value) {

							$row 	= array();
							$i 		+= 1;

						array_push($data,[
											$i,
											$value->user_id, 
											$value->username,
											
											"<button class='btn btn-info btn-xs' id='add2' data-id='".$value->user_id."'>Add to Regular</button>"
									]);	
				}

			}
			else {

					$data[] = [''] ;

			}

		$output = array(
							"data" => $data,
						);  

	 	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}


public function addProfit1() {
			$result = [] ;

			$insertCredit = [] ;
			$updateWallet = [] ;


			if( $this->is_logged_in() ) {

					


					$this->load->model('model_profitshare');

					
					$date 	= date('Y-m-d');

					$profits = [
									'com_percent' 	=>	$this->input->post('profit'),
									'posted_date'	=> $date
								];

				

					if( $this->model_profitshare->insert( $profits ) ){

						$profit 	= $this->input->post('profit') / 2;
						$date 		= date('Y-m-d');
						 foreach ($this->model_profit_share_all->select('user_id',['date' => $date]) as $key => $p) {
						 
							foreach ($this->model_lifejacket_subscription->select('user_id,amount,lifejacket_id,sponsor,remark',['user_id' => $p->user_id]) as $key => $value) {
										$user_id 			= 	$value->user_id;
									    $amounts 			= 	$value->amount;
									    $lifejacket_id 		=	$value->lifejacket_id;
									    $lifejacket_sponsor = 	$value->sponsor;
									    $lifejacket_remark	= 	$value->remark;

										 if($lifejacket_remark=='Coin Purchase')
										 {
										        $newdesc=$lifejacket_sponsor." Coin" . " " . $amounts;
										    }
										    else
										    {
										        $newdesc=$lifejacket_id." Package" . " " . $amounts;
										    }
										    $profits=0;
										    $xc=0.10;
										    if($amounts>=5000)
										    {
										       $profits=$profit;
										    }
										    else
										    {
										       $profits=$profit;
										    }

										     	$rwallet 		= $value->amount*$profits/100;
    											$invoice_no 	= $user_id.rand(10000,99999);

    									
    									if($rwallet> 0) {

    										$chald  = $this->model_acc_close_request->count_ref(['lfjid'=>$lifejacket_id,'user_id'=>$user_id]);


    										 if($chald>0)
										        {}
										        else
										        {

										        	$ewalletbalance = 0 ;
										        	$ams = 0 ;
										        	
										        	 $urls="https://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];



										        	 foreach ($this->model_final_e_wallet->select('amount',['user_id'=>$user_id]) as $key => $bal) {	
										        	 			$ewalletbalance =  $bal->amount;
										        	 	}
										        	 	
										        	 			 //array_push($updateWallet,[
													        	 	// 						'user_id' => $user_id,
													        	 	// 						'amount'  => $ewalletbalance + $rwallet ,
													        	 	// 					  ]);
													        	 	
													        	 	$ams = 	  $ewalletbalance + $rwallet;
													        	 	
													        	 	if($ams !=0 && $ewalletbalance !=0) {
													        	 	    
													        	 	     array_push($updateWallet,[
													        	 							'user_id' => $user_id,
													        	 							'amount'  => ($ewalletbalance + $rwallet) ,
													        	 						  ]);
													        	 	}
													        	 	
													        	 						  
														        	array_push($insertCredit,[
																		        				'transaction_no'	=> $invoice_no,
																		        				'user_id'			=> $user_id,
																		        				'credit_amt'		=> $rwallet,
																		        				'receiver_id'		=> $user_id,
																		        				'sender_id'			=> '123456',
																		        				'debit_amt'			=> 0,
																		        				'admin_charge'		=> 0,
																		        				'invoice_no'		=> $invoice_no,
																		        				'receive_date'		=> $date,
																		        				'ttype'				=> 'Profit Sharing Bonus',
																		        				'TranDescription'	=> $newdesc,
																		        				'Cause'				=> 'Commission of MYR '.$rwallet.' For Package '.$amounts,
																		        				'Remark'			=> $amounts,
																		        				'product_name'		=> 'Bonus From Profit',
																		        				'status'			=> '0',
																		        				'ewallet_used_by'	=> 'Withdrawal Wallet',
																		        				'current_url'		=> $urls 
																        					]
														        					);
										        }
    									}
							}
						}
									$this->updateWallet($updateWallet);
									$this->insertCredit($insertCredit);

									array_push($result ,[

															'title' 	=>	'Add Sucess',
															'text'		=> 	'Todays profit has been added for regular members',
															'type'		=>	'success'
												 		]) ;
										
						}
						else {
									array_push($result , [
															'title' 	=>'Add Failed',
															'text'		=> 'Something went wrong',
															'type'		=>'error'
														]);

							}


			}

			echo json_encode($result);

			

	}


	public function addProfit2() {
			$result = [] ;

			$insertCredit = [] ;
			$updateWallet = [] ;


			if( $this->is_logged_in() ) {

			

					$this->load->model('model_profitshare');

					
					$date 	= date('Y-m-d');

					$profits = [
									'com_percent' 	=>	$this->input->post('profit'),
									'posted_date'	=> $date
								];

				

					if( $this->model_profitshare->insert( $profits ) ){

						$profit 	= $this->input->post('profit') / 2;
						$date 		= date('Y-m-d');
						 foreach ($this->model_profit_share_exclude->select('user_id',['date' => $date]) as $key => $p) {
						     
							foreach ($this->model_lifejacket_subscription->select('user_id,amount,lifejacket_id,sponsor,remark',['user_id' => $p->user_id]) as $key => $value) {
										$user_id 			= 	$value->user_id;
									    $amounts 			= 	$value->amount;
									    $lifejacket_id 		=	$value->lifejacket_id;
									    $lifejacket_sponsor = 	$value->sponsor;
									    $lifejacket_remark	= 	$value->remark;

										 if($lifejacket_remark=='Coin Purchase')
										 {
										        $newdesc=$lifejacket_sponsor." Coin" . " " . $amounts;
										    }
										    else
										    {
										        $newdesc=$lifejacket_id." Package" . " " . $amounts;
										    }
										    $profits=0;
										    $xc=0.10;
										    if($amounts>=5000)
										    {
										       $profits=$profit;
										    }
										    else
										    {
										       $profits=$profit;
										    }

										     	$rwallet 		= $value->amount*$profits/100;
    											$invoice_no 	= $user_id.rand(10000,99999);
    											
    											

    									
    									if($rwallet> 0) {

    										$chald  = $this->model_acc_close_request->count_ref(['lfjid'=>$lifejacket_id,'user_id'=>$user_id]);


    										 if($chald>0)
										        {}
										        else
										        {

										        	   $ewalletbalance = 0 ;
										        	$ams = 0 ;
										        	 $urls="https://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];



										        	 foreach ($this->model_final_e_wallet->select('amount',['user_id'=>$user_id]) as $key => $bal) {	
										        	 			$ewalletbalance =  $bal->amount;
										        	 	}

										        		
										        	 			 //array_push($updateWallet,[
													        	 	// 						'user_id' => $user_id,
													        	 	// 						'amount'  => $bal->amount + $rwallet ,

													        	 	// 					  ]);
										        		
										        		    $ams = $ewalletbalance +  $rwallet;
										        		    
										        		    
										        		    if($ams != 0 && $ewalletbalance !=0) {
										        		        	//$this->model_final_e_wallet->query("UPDATE final_e_wallet set amount='".$ams."' WHERE user_id='".$user_id."'") ;
										        		   
										        		        
										        		                    
										        	 			    array_push($updateWallet,[
													        	 							    'user_id' => $user_id,
													        	 							    'amount'  => ($ewalletbalance + $rwallet) ,

													        	 						  ]);
										        		    }
										        	    

														        	array_push($insertCredit,[

																		        				'transaction_no'	=> $invoice_no,
																		        				'user_id'			=> $user_id,
																		        				'credit_amt'		=> $rwallet,
																		        				'receiver_id'		=> $user_id,
																		        				'sender_id'			=> '123456',
																		        				'debit_amt'			=>0,
																		        				'admin_charge'		=>0,
																		        				'invoice_no'		=> $invoice_no,
																		        				'receive_date'		=> $date,
																		        				'ttype'				=> 'Profit Sharing Bonus',
																		        				'TranDescription'	=> $newdesc,
																		        				'Cause'				=> 'Commission of MYR '.$rwallet.' For Package '.$amounts,
																		        				'Remark'			=> $amounts,
																		        				'product_name'		=> 'Bonus From Profit',
																		        				'status'			=> '0',
																		        				'ewallet_used_by'	=> 'Withdrawal Wallet',
																		        				'current_url'		=> $urls 
																        					]

														        					);
										        }
    									}
										
							}
							
							
							    //$this->model_profit_share_exclude->update(['is_paid'=>1],['user_id'=>$p->user_id]);

						}
									$this->updateWallet($updateWallet);
									$this->insertCredit($insertCredit);

									array_push($result ,[

															'title' 	=>	'Add Sucess',
															'text'		=> 	'Todays profit has been added Irregular members',
															'type'		=>	'success'
												 		]) ;
										
						}
						else {
									array_push($result , [
															'title' 	=>'Add Failed',
															'text'		=> 'Something went wrong',
															'type'		=>'error'
														]);
							}


			}

			echo json_encode($result);

			

	}


	public function insertCredit($data = []){


				$this->db->insert_batch('credit_debit', $data); 

			}

	public function updateWallet($data = [] ) {

			$this->db->update_batch('final_e_wallet',$data, 'user_id'); 

	}
    
    public function searchwallet() {
        
                            $result = [
                                            'user_id' => '' ,
                                        	'credit' => '',
											'debit' => '',
											'balance' => ''
                                    ] ;
					$data = [
								'credit_debit'	=> $result ,
								'user'          => '',
								'msg'           => ''
							];

                	return $this->load->view('member_management/search_wallet',$data);

    }
    
    public function search_wallet () {
        
        $user_id = $this->input->post('user');
                  
    	$withdrawals = [] ;
		$etorwallet  = [] ;
		$ewallet     = [] ;
		$autoeallet  = [] ;

			foreach ($this->model_credit_amt->query("Select sender_id,receiver_id,ttype,credit_amt,debit_amt,ts from credit_debit where user_id='".$user_id."'  AND (ttype ='Profit Sharing Bonus' OR ttype='Royalty Bonus' OR ttype='Unilevel Bonus' OR ttype='Referral Bonus' OR ttype='Admin Credited') order by ts desc")->result() as $key => $value1) {
						
						array_push($etorwallet,[    
						                          'sender_id'   => $value1->sender_id,
						                          'receiver_id' => $value1->receiver_id,
												  'ttype'       => $value1->ttype,
												  'debit_amt'   => $value1->debit_amt,
												  'credit_amt'  => $value1->credit_amt,
												  'date'		=> $value1->ts
												]);

			}
			
			foreach($this->model_credit_amt->query("SELECT credit_amt,debit_amt,ts FROM credit_debit WHERE user_id='".$user_id."' AND ttype='Fund Transfer' AND sender_id='".$user_id."' AND receiver_id='".$user_id."' AND debit_amt <> 0 ")->result() as $key => $val) {
			                array_push($ewallet,
			                                    [
			                                         'credit_amt'  => $val->credit_amt,
			                                         'debit_amt'   => $val->debit_amt,
			                                         'ts'          => $val->ts
		                                        ]
			                            );
			}
            foreach($this->model_credit_amt->query("Select amount from final_e_wallet2018042018 where user_id='".$user_id."'")->result() as $key => $vals) {
                    
                        $autoeallet = $vals->amount;
                        
                        
                
            }
			 foreach ($this->model_credit_amt->query("Select total_paid_amount,admin_remark,posted_date,admin_response_date from withdraw_request where user_id='".$user_id."' ")->result() as $key => $value) {
			 			array_push($withdrawals,[
				 									'amount'      => $value->total_paid_amount,
				 									'posted_date' => $value->posted_date,
				 									'admin_response_date' => $value->admin_response_date
			 								    ]) ;
			 		  
			 }
			 
				 $data = [
			 				'ewallet'   => $etorwallet,
			 				'withdrawals' => $withdrawals,
			 				'wallet'        => $ewallet,
			 				'autoeallet'   => $autoeallet
			 			];


					return $this->load->view('member_management/search_wallet',$data);
    }
    
    public function summary() {
        
                $result = [] ;
        
                 foreach($this->model_users->select("user_id,username",['user_rank_name <>' => 'Normal User']) as $key => $value) {
                            $profits    = 0 ;
                            $withdraws  = 0 ;
                            $etransfer  = 0 ;
                    foreach ($this->model_credit_amt->query("Select sum(credit_amt) as total_profits from credit_debit where user_id='".$value->user_id."'  AND (ttype ='Profit Sharing Bonus' OR ttype='Royalty Bonus' OR ttype='Unilevel Bonus' OR ttype='Referral Bonus') order by ts desc")->result() as $key => $value1) {
                                    $profits = $value1->total_profits;
			                   }
			
			        foreach($this->model_credit_amt->query("SELECT sum(debit_amt) as total_ertransfer FROM credit_debit WHERE user_id='".$value->user_id."' AND ttype='Fund Transfer' AND sender_id='".$value->user_id."' AND receiver_id='".$value->user_id."' AND debit_amt <> 0 ")->result() as $key => $val) {
			                        $etransfer = $val->total_ertransfer;
		                	}

			        foreach ($this->model_credit_amt->query("Select sum(total_paid_amount) as total_withdraw from withdraw_request where user_id='".$value->user_id."' AND status <> 2 ")->result() as $key => $value2) {
			 			             $withdraws =$value2->total_withdraw;
			                }
                            array_push($result,[
                                                    'user_id'   => $value->user_id,
                                                    'username'  => $value->username,
                                                    'profits'   => $profits,
                                                    'withdraw'  => $withdraws,
                                                    'etransfer' => $etransfer
                                                ]);
                     
                 }
                        
                    $data = ['summary' => $result ] ;
        
            	return $this->load->view('member_management/summary',$data);
        
    }
    
    public function updatewalletnow () {
        
                
            	$user_id = $this->input->post('user_id');
            	$wallet  =  $this->input->post('amount');
                    	
                $msg    = "" ;
                $result = [] ;
				

            if(is_numeric($wallet)) {
                    	if( $this->model_final_e_wallet->update(['amount' => $wallet],['user_id' => $user_id] ) ) {
            	                $msg = "success" ;
                    	}
                    	else {
                    	        $msg = "" ;
                    	}
                }
                else {
                             $msg = "" ;
                    
                }
            	
            
            	
            	foreach ($this->model_credit_amt->query("SELECT SUM(credit_amt) AS credit,SUM(debit_amt) AS debit,(SUM(credit_amt) - SUM(debit_amt)) as balance FROM credit_debit WHERE user_id='".$user_id."'")->result() as $key => $value) {
				

					        array_push($result,[
    					                         'user_id'       => $user_id,
    											 'credit' => $value->credit,
    											 'debit' => $value->debit,
    											 'balance' => $value->balance
										    ]) ;
					}

					$data = [
								'credit_debit'	=> $result,
								'user'          => $user_id,
								'msg'           => $msg
							];
            	
           
            return $this->load->view('member_management/search_wallet',$data);
        
    }


}