<?php
defined('BASEPATH') or exit('No direct script access allowed');



class MemberlistController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		

	}

	public function index () {
			
				if($this->is_logged_in() ) {
						
					return $this->load->view('member_management/memberlist');
				}
				else {

					redirect('login');
				}
	}



	public function getList(){


			$data 		 = array();
			$status 	 = "" ;
			$i 			 = 0 ;
			$user_status = "" ;

			if( $this->is_logged_in() ){

				foreach ($this->model_users->select('id,user_id,email,user_rank_name,registration_date,user_status') as $key => $value) {

							$row 	= array();
							$i 		+= 1;

						if( $this->model_lifejacket_subscription->count_ref(['user_id'=>$value->user_id] ) > 0){

							$status = "Paid" ;
						}
						else {

							$status = "Unpaid";
						}
						if($value->user_status=='0') {
							$user_status = '<button  class="btn btn-danger btn-xs" id="deactivate" data="'.$value->id.'" value="'.$value->user_id.'"><i class="fa fa-trash-o"></i> Deactivate </button>';

						}
						elseif($value->user_status=='1'){
								$user_status = '<button  class="btn btn-info btn-xs" id="activate" data="'.$value->id.'" value="'.$value->user_id.'"><i class="fa fa-check"></i> Activate </button>';
						}

					array_push( $data ,
										[ 
											$i,
											$value->user_id, 
											$value->email,
											$value->user_rank_name,
											$value->registration_date,
											$status,
											'<a href="'.site_url().'member/edit/'.$value->id.'" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-pencil"></i> Edit </a>' .' '.$user_status 
										]
									);		
				}

			}
			else {

					$data[] = [''] ;

			}

		$output = array(
							"data" => $data,
						);  
			       


	 	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}


	public function activateUser(){


		$id 	= $this->input->post('id');

		$data 	= [] ;

			$result  = $this->model_users->update(['user_status'=>'0'],['id'=>$id]);

			if($result) {
					$data = [
								'status' => 'ok',
								'msg'=>'User has been activated'
							];
			}
			else {
				$data = [
							'status' => 'error', 
							'msg'	=>'Something went wrong'
						];
			}

		echo json_encode($data);
	}
	public function deactivateUser(){


		$id 	= $this->input->post('id');

		$data 	= [] ;


			$result  = $this->model_users->update(['user_status'=>'1'],['id'=>$id]);

			if($result) {
							$data = [
										'status' => 'ok',
										'msg'=>'User has been deactivated'
									];
			}
			else {

							$data = [
										'status' => 'error', 
										'msg'	=>'Something went wrong'
									];
			}
		echo json_encode($data);
	}


	public function editUser(){


		

		
		if($this->is_logged_in() ) {
				$id = $this->uri->segment(3);
				$sponsor_id = "" ;


							foreach ($this->model_users->select('ref_id',['id' => $id]) as $key => $value) {
										$sponsor_id = $value->ref_id;
							}

						$data = [
									'sponsor' 		  => $this->model_users->select('user_id,first_name',['user_id'=>$sponsor_id]),
									'member_profile'  => $this->model_users->select('*',['id'=>$id])
								];
						
					return $this->load->view('member_management/editView',$data);
				}
				else {

					redirect('login');
			}



	}



	public function updateinfo() {



				if($this->is_logged_in() ) {

						$this->load->helper(array('form', 'url'));
                		$this->load->library('form_validation');
                		$this->load->model('examples/validation_callables');


						$result = [] ;

						$user_id   		= 	$this->input->post('user_id');
						$username   	= 	$this->input->post('username');
						$first_name 	=	$this->input->post('first_name');
						$last_name 		= 	$this->input->post('last_name');
						$telephone 		=	$this->input->post('telephone');
						$address 		=	$this->input->post('address');
						$country 		=	$this->input->post('country');
						$state 			=	$this->input->post('state');
						$city 			=	$this->input->post('city');
						$passwd			=	$this->input->post('passwd');
						$t_code 		=	$this->input->post('t_code');
						$dob			=	$this->input->post('dob');
						$gender 		=	$this->input->post('sex');


						//check there's changes made in username and telephone
						if( $this->model_users->count_ref(['user_id'=>$user_id,'username'=>$username]) <= 0 || $this->model_users->count_ref(['user_id'=>$user_id,'telephone'=>$telephone]) <=0 ) {

										 $user_data = [	
										 
														 'username'      => $username,	
														 'passwd'		 => $passwd			 
											];
										
							$validation_rules = [
													
													[
														'field' => 'passwd',
														'label' => 'passwd',
														'rules' => [
															'trim',
															'required',
															[ 
																'_check_password_strength', 
																[ $this->validation_callables, '_check_password_strength' ] 
															]
														],
														'errors' => [
															'required' => 'The password field is required.'
														]
													],
													[
														'field'  => 'username',
														'label'  => 'username',
														'rules'  => 'trim|required|valid_email|is_unique[user_registration.username]',
														'errors' => [
															'is_unique' => 'Username address already in use.'
														]
													],
											];

									$this->form_validation->set_data( $user_data );
									$this->form_validation->set_rules( $validation_rules );

									if( $this->form_validation->run()  ) {

																$profie_data = [
																					
																					'username'   	=> 	$this->input->post('username'),
																					'first_name' 	=>	$this->input->post('first_name'),
																					'last_name '	=> 	$this->input->post('last_name'),
																					'telephone '	=>	$this->input->post('telephone'),
																					'address' 		=>	$this->input->post('address'),
																					'country' 		=>	$this->input->post('country'),
																					'state' 		=>	$this->input->post('state'),
																					'city' 			=>	$this->input->post('city'),
																					'passwd'		=>	$this->input->post('passwd'),
																					't_code' 		=>	$this->input->post('t_code'),
																					'dob'			=>	$this->input->post('dob'),
																					'sex' 			=>	$this->input->post('sex'),
																		];


														if( $this->model_users->update( $profie_data, ['user_id'=>$user_id ] ) ) {

																	array_push($result,[

																 					'title' 	=>'Update Sucess',
																 					'text'		=> 'Userid '. $user_id . ' info has been updated',
																 					'type'		=>'success'
											 							]);
														}
														else {


																	array_push($result,[

																	 					'title' 	=>'Update Failed',
																	 					'text'		=> 'Something went wrong',
																	 					'type'		=>'error'
																 					]);

														}
									
									}

									else {

												array_push($result,[

												 					'title' 	=>'Update Failed',
												 					'text'		=> validation_errors(),
												 					'type'		=>'error'
											 					]);
									}

								}

						//if there's no changes made in username and telephone 
						else {

							 $user_data2 = [	
											
											 'passwd'		 => $passwd
										 
							];
						 
						


							$validation_rules2 = [
												
													[
														'field' => 'passwd',
														'label' => 'passwd',
														'rules' => [
															'trim',
															'required',
															[ 
																'_check_password_strength', 
																[ $this->validation_callables, '_check_password_strength' ] 
															]
														],
														'errors' => [
															'required' => 'The password field is required.'
														]
													],
													
											];
										$this->form_validation->set_data( $user_data2 );
										$this->form_validation->set_rules( $validation_rules2 );



										if( $this->form_validation->run()  ) {

																$profie_data = [
																						
																						'username'   	=> 	$this->input->post('username'),
																						'first_name' 	=>	$this->input->post('first_name'),
																						'last_name '	=> 	$this->input->post('last_name'),
																						'telephone '	=>	$this->input->post('telephone'),
																						'address' 		=>	$this->input->post('address'),
																						'country' 		=>	$this->input->post('country'),
																						'state' 		=>	$this->input->post('state'),
																						'city' 			=>	$this->input->post('city'),
																						'passwd'		=>	$this->input->post('passwd'),
																						't_code' 		=>	$this->input->post('t_code'),
																						'dob'			=>	$this->input->post('dob'),
																						'sex' 			=>	$this->input->post('sex'),
																			];


															if( $this->model_users->update( $profie_data, ['user_id'=>$user_id] ) ) {

																		array_push($result,[

																		 					'title' 	=>'Update Sucess',
																		 					'text'		=> 'Userid '. $user_id . ' info has been updated',
																		 					'type'		=>'success'
													 									]);
															}
															else {


																		array_push($result,[

																		 					'title' 	=>'Update Failed',
																		 					'text'		=> 'Something went wrong',
																		 					'type'		=>'error'
																	 					]);

															}

										
										}

										else {

													array_push($result,[

													 					'title' 	=>'Update Failed',
													 					'text'		=> validation_errors(),
													 					'type'		=>'error'
												 					]);
										}



								}

				

						echo json_encode($result,JSON_UNESCAPED_SLASHES);

						
				}

	}

	public function updatebank() {


	
				


				if($this->is_logged_in() ) {



		

					$user_id   		= 	$this->input->post('user_id');
					$bank_details =  [
										
										'acc_name' 		=>	$this->input->post('acc_name'),
										'ac_no'			=>	$this->input->post('ac_no'),
										'bank_nm' 		=>	$this->input->post('bank_nm'),
										'branch_nm'	 	=>	$this->input->post('branch_nm'),
										'swift_code' 	=>	$this->input->post('swift_code'),
										'bitcoin' 		=>	$this->input->post('bitcoin'),
										'ethereum' 		=>	$this->input->post('ethereum'),
										'ripple' 		=>	$this->input->post('ripple'),
										'ripple_tag' 	=>	$this->input->post('ripple_tag'),
										'ethereumc' 	=>	$this->input->post('ethereumc'), 

									];


							 $result = [] ;

														if( $this->model_users->update( $bank_details, ['user_id'=>$user_id] ) ) {

																	array_push($result ,[

																	 					'title' 	=>'Update Sucess',
																	 					'text'		=> 'Userid '. $user_id . ' bank details has been updated',
																	 					'type'		=>'success'
												 									]) ;


																	
														}
														else {


																	array_push($result , [

																	 					'title' 	=>'Update Failed',
																	 					'text'		=> 'Something went wrong',
																	 					'type'		=>'error'
																 					]);


																

														}

					echo json_encode($result);
									

				}


					

	}


}