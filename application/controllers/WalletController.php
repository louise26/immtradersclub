<?php
defined('BASEPATH') or exit('No direct script access allowed');



class WalletController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_add_fund_request');
		$this->load->model('model_credit_amt');

	}

	public function index () {
			
				if($this->is_logged_in() ) {	
					return $this->load->view('wallet/managewallet');
				}
				else {

					redirect('login');
				}
	}

public function getList(){


			$data 		 = array();
			$status 	 = "" ;
			$i 			 = 0 ;
			$user_status = "" ;

			if( $this->is_logged_in() ){

				foreach ($this->model_users->select('user_registration.id,user_registration.username,user_registration.user_id,user_registration.first_name,user_registration.last_name,final_reg_wallet.amount',[],['final_reg_wallet'=>'user_registration.user_id=final_reg_wallet.user_id']) as $key => $value) {

							$row 	= array();
							$i 		+= 1;

					array_push($data,[

									$i,
									$value->user_id, 
									$value->username,
									$value->first_name .' ' .$value->last_name,
									$value->amount,
									'<center><a href="'.site_url().'wallet/manage-rwallet/admin-ts/'.$value->user_id.'" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-eye"></i> View </a></center>',
									'<center><a href="'.site_url().'wallet/manage-rwallet/manage/'.$value->user_id.'" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-cog"> </i> Manage </a></center>',
									'<center><a href="'.site_url().'wallet/manage-rwallet/history/'.$value->user_id.'" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-eye"></i> View </a></center>'

								]);	
				}

			}
			else {

					$data[] = [''] ;

			}

		$output = array(
							"data" => $data,
						);  
			       

	 	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}
	
	public function admints() {

			if( $this->is_logged_in() ){
				
				return $this->load->view('wallet/admints');
			}
			else {

					redirect('login');
			}
	}
public function wallethistory() {

			if( $this->is_logged_in() ){
				
				return $this->load->view('wallet/history');
			}
			else {

					redirect('login');
			}
	}

	public function getadmints(){

				if( $this->is_logged_in() ){

					$user_id 	= $this->uri->segment(3);
					$data    	= [];
					$sendername = "";
					$user 		= "";

					$i = 0 ;

					foreach ($this->model_credit_amt->query("SELECT user_id,sender_id,ttype,receive_date,credit_amt,debit_amt from credit_debit where user_id='".$user_id."' and (Remark='Fund Deducted By Admin' || Remark='Fund Credited By Admin') ")->result() as $key => $value) {
						
						$i +=1;
						foreach ($this->model_users->select('username',['user_id'=>$value->user_id]) as $key => $value1) {
									
									$user = $value1->username;
						}
						
						foreach ($this->model_users->select('username',['user_id'=>$value->sender_id]) as $key => $value2) {
									
									$sendername = $value2->username;
						}


						array_push($data,
										 [
										 	$i,
										 	$value->user_id,
										 	$user,
										 	$value->sender_id,
										 	$sendername,
										 	$value->ttype,
										 	$value->credit_amt,
										 	$value->debit_amt,
										 	date('F d, Y',strtotime($value->receive_date))
										 ]	
										);

					}
				}

					$output = array(
							"data" => $data,
						);  
			       

	 	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}
	public function getRhistory(){

				if( $this->is_logged_in() ){

					$user_id 	= $this->uri->segment(3);
					$data    	= [];
					$sendername = "";
					$user 		= "";

					$i = 0 ;
					//$select = "*", $where = [], $join = [], $order_by = [], $limit = []
					foreach ($this->model_credit_amt->query("SELECT user_id,sender_id,ttype,receive_date,credit_amt,debit_amt from credit_debit where user_id='".$user_id."' order by id desc limit 5000 ")->result() as $key => $value) {
						
						$i +=1;
						foreach ($this->model_users->select('username',['user_id'=>$value->user_id]) as $key => $value1) {
									
									$user = $value1->username;
						}
						
						foreach ($this->model_users->select('username',['user_id'=>$value->sender_id]) as $key => $value2) {
									
									$sendername = $value2->username;
						}


						array_push($data,
										 [
										 	$i,
										 	$value->user_id,
										 	$user,
										 	$value->sender_id,
										 	$sendername,
										 	$value->ttype,
										 	$value->credit_amt,
										 	$value->debit_amt,
										 	date('F d, Y',strtotime($value->receive_date))
										 ]	
										);

					}


				}

					$output = array(
							"data" => $data,
						);  
	 	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}


	public function managewallet() {

			if( $this->is_logged_in() ){

				$user_id = $this->uri->segment(4);

				$data = [] ;

				foreach($this->model_final_reg_wallet->select('amount' , ['user_id'=>$user_id]) as $key => $value){

					$data = [

							'balance' => $value->amount
					];
					
				}
				return $this->load->view('wallet/managerwallet',$data);
			}
			else {
					redirect('login');
			}
	}
	public function updateRwallet(){

		if( $this->is_logged_in() ){

			$user_id =  $this->input->post('user_id');
			$amount  = 	$this->input->post('amount');
			$remark  =	$this->input->post('remark');

			$wallet_balance = 0 ;

			foreach($this->model_final_reg_wallet->select('amount' , ['user_id'=>$user_id]) as $key => $value){
					$wallet_balance = $value->amount;
			}

			$new_balance = $amount +  $wallet_balance ;

			$rand=$user_id.rand(00001,99999);
			$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

			/** insert data 
			* transaction_no
			* user_id
			* credit_amt
			* debit_amt
			* admin_charge
			* receiver_id
			* sender_id
			* receive_date
			* ttype
			* TranDescription
			* Cause
			* Remark
			* invoice_no
			* product_name
			* status
			* ewallet_used_by
			* ts
			* current_url
			*/

				$result = [] ;
				$data =   [
							'transaction_no' 	=> 	$rand,
							'user_id'			=>	$user_id,
							'credit_amt'		=>	$amount,
							'debit_amt'			=>	0,
							'admin_charge'		=>	0,
							'receiver_id'		=>	$user_id,
							'sender_id'			=> '123456',
							'receive_date'		=>	date('Y-m-d'),
							'ttype'				=>	'Fund Transfer',
							'TranDescription'	=>	$remark,
							'Cause'				=>	'Fund Credited By Admin',
							'Remark'			=>	'Fund Credited By Admin',
							'invoice_no'		=>	$rand,
							'product_name'		=>	'Fund Credited',
							'status'			=>	0,
							'ewallet_used_by'	=>	'Withdrawal Wallet',
							'ts'				=> 	date('Y-m-d H:i:s'),
							'current_url'		=> $urls
						];

				if( $this->model_credit_amt->insert( $data ) ){
					if( $this->model_final_reg_wallet->update( [ 'amount' => $new_balance ], [ 'user_id' => $user_id ] ) ){
						array_push($result  , [
									'title' 		=>	'Add Fund Sucess',
									'text'			=> 	'$ ' . $amount . ' was successfully credited to ' . $user_id . ' R-Wallet',
									'type'			=>	'success' ,
									'new_balance'	=>	$new_balance
								] );
						}
						else {
							array_push($result  , [
								'title' 		=>	'Add Fund Failed',
								'text'			=> 	$this->db->error(),
								'type'			=>	'error' ,
								'new_balance'	=>	 $new_balance
							]) ;
	
						}
				}
				else  {
					array_push($result  , [
							'title' 		=>	'Add Fund Failed',
							'text'			=> 	 $this->db->error(),
							'type'			=>	'error' ,
							'new_balance'	=>	$new_balance
					] );
				}
			
			

			
			echo json_encode($result);

		}

		else {
				redirect('login');
		}
	}



	public function deductRwallet(){

		if( $this->is_logged_in() ){

			$user_id =  $this->input->post('user_id');
			$amount  = 	$this->input->post('amount');
			$remark  =	$this->input->post('remark');

			$wallet_balance = 0 ;

			foreach($this->model_final_reg_wallet->select('amount' , ['user_id'=>$user_id]) as $key => $value){
					$wallet_balance = $value->amount;
			}

			$new_balance = $wallet_balance - $amount ;

			$rand=$user_id.rand(00001,99999);
			$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

			/** insert data 
			* transaction_no
			* user_id
			* credit_amt
			* debit_amt
			* admin_charge
			* receiver_id
			* sender_id
			* receive_date
			* ttype
			* TranDescription
			* Cause
			* Remark
			* invoice_no
			* product_name
			* status
			* ewallet_used_by
			* ts
			* current_url
			*/

				$result = [] ;
				$data =   [
							'transaction_no' 	=> 	$rand,
							'user_id'			=>	$user_id,
							'credit_amt'		=>	0,
							'debit_amt'			=>	$amount,
							'admin_charge'		=>	0,
							'receiver_id'		=>	$user_id,
							'sender_id'			=> '123456',
							'receive_date'		=>	date('Y-m-d'),
							'ttype'				=>	'Fund Transfer',
							'TranDescription'	=>	$remark,
							'Cause'				=>	'Fund Deducted By Admin',
							'Remark'			=>	'Fund Deducted By Admin',
							'invoice_no'		=>	$rand,
							'product_name'		=>	'Fund Deducted',
							'status'			=>	0,
							'ewallet_used_by'	=>	'Withdrawal Wallet',
							'ts'				=> 	date('Y-m-d H:i:s'),
							'current_url'		=> $urls
						];

			

				if( $this->model_credit_amt->insert( $data ) ){
					if( $this->model_final_reg_wallet->update( [ 'amount' => $new_balance ], [ 'user_id' => $user_id ] ) ){
						array_push($result  , [
									'title' 		=>	'Add Fund Sucess',
									'text'			=> 	'$ ' . $amount . ' was successfully deducted to ' . $user_id . ' R-Wallet',
									'type'			=>	'success' ,
									'new_balance'	=>	$new_balance
								] );
						}
						else {
							array_push($result  , [
								'title' 		=>	'Add Fund Failed',
								'text'			=> 	$this->db->error(),
								'type'			=>	'error' ,
								'new_balance'	=>	 $new_balance
							]) ;
	
						}
				}
				else  {
					array_push($result  , [
							'title' 		=>	'Add Fund Failed',
							'text'			=> 	 $this->db->error(),
							'type'			=>	'error' ,
							'new_balance'	=>	$new_balance
					] );
				}
			
			

			
			echo json_encode($result);

		}

		else {
				redirect('login');
		}
	}

	public function ewallet(){
		if($this->is_logged_in() ) {	
			return $this->load->view('wallet/ewallet');
		}
		else {

			redirect('login');
		}
	}
	public function getewalletList(){
			$data 		 = array();
			$status 	 = "" ;
			$i 			 = 0 ;
			$user_status = "" ;

			if( $this->is_logged_in() ){

				foreach ($this->model_users->select('user_registration.id,user_registration.username,user_registration.user_id,user_registration.first_name,user_registration.last_name,final_e_wallet.amount',['user_registration.user_rank_name <>' => 'Normal User'],['final_e_wallet'=>'user_registration.user_id=final_e_wallet.user_id']) as $key => $value) {

							$row 	= array();
							$i 		+= 1;

					array_push($data,[

									$i,
									$value->user_id, 
									$value->username,
									$value->first_name .' ' .$value->last_name,
									$value->amount,
									'<center><a href="'.site_url().'wallet/manage-ewallet/admin-ts/'.$value->user_id.'" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-eye"></i> View </a></center>',
									'<center><a href="'.site_url().'wallet/manage-ewallet/manage/'.$value->user_id.'" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-cog"> </i> Manage </a></center>',
									'<center><a href="'.site_url().'wallet/manage-ewallet/history/'.$value->user_id.'" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-eye"></i> View </a></center>'

								]);	
				}

			}
			else {

					$data[] = [''] ;

			}

		$output = array(
							"data" => $data,
						);  
				

		echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}
	public function managusrewallet() {

			if( $this->is_logged_in() ){

				$user_id = $this->uri->segment(4);

				$data = [] ;

				foreach($this->model_final_e_wallet->select('amount' , ['user_id'=>$user_id]) as $key => $value){

					$data = [
						    	'balance' => $value->amount
					        ];
				}
				return $this->load->view('wallet/manageewallet',$data);
			}
			else {
					redirect('login');
			}
	}
	public function updateEwallet(){

		if( $this->is_logged_in() ){

			$user_id =  $this->input->post('user_id');
			$amount  = 	$this->input->post('amount');
			$remark  =	$this->input->post('remark');

			$wallet_balance = 0 ;

			foreach($this->model_final_e_wallet->select('amount' , ['user_id'=>$user_id]) as $key => $value){
					$wallet_balance = $value->amount;
			}

			$new_balance = $amount +  $wallet_balance ;

			$rand=$user_id.rand(00001,99999);
			$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

			/** insert data 
			* transaction_no
			* user_id
			* credit_amt
			* debit_amt
			* admin_charge
			* receiver_id
			* sender_id
			* receive_date
			* ttype
			* TranDescription
			* Cause
			* Remark
			* invoice_no
			* product_name
			* status
			* ewallet_used_by
			* ts
			* current_url
			*/

				$result = [] ;
				$data =   [
							'transaction_no' 	=> 	$rand,
							'user_id'			=>	$user_id,
							'credit_amt'		=>	$amount,
							'debit_amt'			=>	0,
							'admin_charge'		=>	0,
							'receiver_id'		=>	$user_id,
							'sender_id'			=>  '123456',
							'receive_date'		=>	date('Y-m-d'),
							'ttype'				=>	'Admin Credited',
							'TranDescription'	=>	$remark,
							'Cause'				=>	'Fund Credited By Admin',
							'Remark'			=>	'Fund Credited By Admin',
							'invoice_no'		=>	$rand,
							'product_name'		=>	'Fund Credited',
							'status'			=>	0,
							'ewallet_used_by'	=>	'Withdrawal Wallet',
							'ts'				=> 	date('Y-m-d H:i:s'),
							'current_url'		=> $urls
						];

				if( $this->model_credit_amt->insert( $data ) ){
				// 	if( $this->model_final_e_wallet->update( [ 'amount' => $new_balance ], [ 'user_id' => $user_id ] ) ){
				// 		array_push($result  , [
				// 					'title' 		=>	'Add Fund Sucess',
				// 					'text'			=> 	'$ ' . $amount . ' was successfully credited to ' . $user_id . ' E-Wallet',
				// 					'type'			=>	'success' ,
				// 					'new_balance'	=>	$new_balance
				// 				] );
				// 		}
				// 		else {
				// 			array_push($result  , [
				// 				'title' 		=>	'Add Fund Failed',
				// 				'text'			=> 	$this->db->error(),
				// 				'type'			=>	'error' ,
				// 				'new_balance'	=>	 $new_balance
				// 			]) ;
				// 		}
				    		array_push($result  , [
									'title' 		=>	'Add Fund Sucess',
									'text'			=> 	'$ ' . $amount . ' was successfully credited to ' . $user_id . ' E-Wallet',
									'type'			=>	'success' ,
									'new_balance'	=>	$new_balance
								] );
				    
				}
				else  {
					array_push($result  , [
							'title' 		=>	'Add Fund Failed',
							'text'			=> 	 $this->db->error(),
							'type'			=>	'error' ,
							'new_balance'	=>	$new_balance
					] );
				}
			
			

			
			echo json_encode($result);

		}

		else {
				redirect('login');
		}
	}

	public function deductEwallet(){

		if( $this->is_logged_in() ){

			$user_id =  $this->input->post('user_id');
			$amount  = 	$this->input->post('amount');
			$remark  =	$this->input->post('remark');

			$wallet_balance = 0 ;

			foreach($this->model_final_e_wallet->select('amount' , ['user_id'=>$user_id]) as $key => $value){
					$wallet_balance = $value->amount;
			}

			$new_balance = $wallet_balance - $amount ;

			$rand=$user_id.rand(00001,99999);
			$urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

			/** insert data 
			* transaction_no
			* user_id
			* credit_amt
			* debit_amt
			* admin_charge
			* receiver_id
			* sender_id
			* receive_date
			* ttype
			* TranDescription
			* Cause
			* Remark
			* invoice_no
			* product_name
			* status
			* ewallet_used_by
			* ts
			* current_url
			*/

				$result = [] ;
				$data =   [
							'transaction_no' 	=> 	$rand,
							'user_id'			=>	$user_id,
							'credit_amt'		=>	0,
							'debit_amt'			=>	$amount,
							'admin_charge'		=>	0,
							'receiver_id'		=>	$user_id,
							'sender_id'			=> '123456',
							'receive_date'		=>	date('Y-m-d'),
							'ttype'				=>	'Fund Transfer',
							'TranDescription'	=>	$remark,
							'Cause'				=>	'Fund Deducted By Admin',
							'Remark'			=>	'Fund Deducted By Admin',
							'invoice_no'		=>	$rand,
							'product_name'		=>	'Fund Deducted',
							'status'			=>	0,
							'ewallet_used_by'	=>	'Withdrawal Wallet',
							'ts'				=> 	date('Y-m-d H:i:s'),
							'current_url'		=> $urls
						];

			

				if( $this->model_credit_amt->insert( $data ) ){
					if( $this->model_final_e_wallet->update( [ 'amount' => $new_balance ], [ 'user_id' => $user_id ] ) ){
						array_push($result  , [
									'title' 		=>	'Add Fund Sucess',
									'text'			=> 	'$ ' . $amount . ' was successfully deducted to ' . $user_id . ' R-Wallet',
									'type'			=>	'success' ,
									'new_balance'	=>	$new_balance
								] );
						}
						else {
							array_push($result  , [
								'title' 		=>	'Add Fund Failed',
								'text'			=> 	$this->db->error(),
								'type'			=>	'error' ,
								'new_balance'	=>	 $new_balance
							]) ;
	
						}
				}
				else  {
					array_push($result  , [
							'title' 		=>	'Add Fund Failed',
							'text'			=> 	 $this->db->error(),
							'type'			=>	'error' ,
							'new_balance'	=>	$new_balance
					] );
				}
			
			

			
			echo json_encode($result);

		}

		else {
				redirect('login');
		}
	}
}
