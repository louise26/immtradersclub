<?php
defined('BASEPATH') or exit('No direct script access allowed');



class WithdrawalController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		// Models loaded
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_add_fund_request');
		$this->load->model('model_credit_amt');
		$this->load->model('model_withdraw_request');

	}

	public function index () {
			
				if( $this->is_logged_in() ) {	

					return $this->load->view('withdrawal/withdrawal');
				}

				else {

					redirect('login');
				}
	}

	public function openwithdrawal() {

		if( $this->is_logged_in() ) {	
						



			$data 		 = array();
			$status 	 = "" ;
			$i 			 = 0 ;
			$user_status = "" ;

			if( $this->is_logged_in() ){

				foreach ($this->model_users->query("Select user_registration.id as id,withdraw_request.id as wid,withdraw_request.user_id,withdraw_request.first_name,withdraw_request.last_name,withdraw_request.request_amount,withdraw_request.transaction_number,withdraw_request.posted_date,withdraw_request.trans_type from withdraw_request JOIN user_registration ON withdraw_request.user_id=user_registration.user_id where withdraw_request.status='0'")->result() as $key => $value) {
							$row 	= array();
							$i 		+= 1;

							array_push($data,
											[
												$i,
												$value->user_id, 
												$value->first_name . ' ' . $value->last_name,
												$value->request_amount,
												($value->request_amount * 60),
												$value->transaction_number,
												$value->trans_type,
												$value->posted_date,
												'<a href="'.site_url().'member/edit/'.$value->id.'" class="btn btn-info btn-xs" target="_blank">View Account Info</a><button  class="btn btn-success btn-xs" id="approve" data-id="'.$value->wid.'" data-amount="'.$value->request_amount.'" value="'.$value->user_id.'" data-trans="'.$value->transaction_number.'"><i class="fa fa-check"></i> Mark As Paid </button><button  class="btn btn-warning btn-xs" id="cancel" data-id="'.$value->wid.'" data-amount="'.$value->request_amount.'" value="'.$value->user_id.'" data-trans="'.$value->transaction_number.'"><i class="fa fa-times"></i> Cancel </button>'
											]);
				}
			}
			else {

					$data[] = [''] ;

			}

			$output = array(
							"data" => $data,
						);  
	 					echo json_encode($output,JSON_UNESCAPED_SLASHES);
			}
		else {

				redirect('login');
		}

	}

	public function closewithdrawal() {

		if($this->is_logged_in() ) {	
						



			$data 		 = array();
			$status 	 = "" ;
			$i 			 = 0 ;
			$user_status = "" ;

			if( $this->is_logged_in() ){

				foreach ($this->model_users->query("Select * from withdraw_request where status ='1'")->result() as $key => $value) {

							$row 	= array();
							$i 		+= 1;

					array_push($data,

									[
										$i,
										$value->user_id, 
										$value->first_name . ' ' . $value->last_name,
										$value->request_amount,
										($value->request_amount * 60),
										$value->transaction_number,
										$value->posted_date,
										$value->admin_response_date,
										$value->admin_remark
									]);

				}

			}
			else {

					$data[] = [''] ;

			}

			$output = array(
							"data" => $data,
						);  
			       
	 					echo json_encode($output,JSON_UNESCAPED_SLASHES);

			}
		else {

				redirect('login');
		}

	}
	public function cancelWithdrawal(){


		$id 			 = $this->input->post('id');
		$user_id 		 = $this->input->post('user_id');
		$transaction_no  = $this->input->post('trans');
		$amount 		 = $this->input->post('amount');
		$date 			 = date('Y-m-d');

		$response 		= [] ;
		$ewallet 		= 0 ;
		$finalbalance   = 0 ;
 	
 		// 	mysqli_query($conn,"UPDATE  withdraw_request set status='1',admin_remark='Paid' where transaction_number='$id'"); 
	


			$data 	= [] ;

			foreach ($this->model_final_e_wallet->select('amount',['user_id'=>$user_id]) as $key => $value) {
				$ewallet = $value->amount;
			}

			$result  = $this->model_withdraw_request->update(['status'=>'2','admin_response_date'=>$date],['user_id'=>$user_id,'transaction_number'=>$transaction_no,'id'=>$id]);

			$finalbalance = $ewallet +  $amount ;
			if($result) {
					
							$this->model_credit_amt->delete(['transaction_no'=>$transaction_no,'user_id'=>$user_id]);

							$this->model_final_e_wallet->update(['amount'=>$finalbalance],['user_id'=>$user_id]);

							$response  = [

											'title' 	=>	'Cancel Sucess',
											'text'		=> 	'Request has been successfully canceled',
											'type'		=>	'success'
										] ;
			}
			else {
							$response = [
											'title' 	=>	'Cancel Failed',
											'text'		=> 	'Something went wrong',
											'type'		=>	'error'
										] ;
			}

		echo json_encode($response);
	}
	public function approvedWithdrawal(){


		$id 			 = $this->input->post('id');
		$user_id 		 = $this->input->post('user_id');
		$transaction_no  = $this->input->post('trans');
		$amount 		 = $this->input->post('amount');
		$date 			 = date('Y-m-d');

		$response 		= [] ;
		$ewallet 		= 0 ;
		$finalbalance   = 0 ;
 	


			$data 	= [] ;


			$result  = $this->model_withdraw_request->update(['status'=>'1','admin_response_date'=>$date,'admin_remark'=>'PAID'],['user_id'=>$user_id,'transaction_number'=>$transaction_no]);

			if($result) {
					
							$response  = [

											'title' 	=>	'Payment Sucess',
											'text'		=> 	'Request has  been paid',
											'type'		=>	'success'
										] ;
			}
			else {
							$response = [
											'title' 	=>	'Payment Failed',
											'text'		=> 	'Something went wrong',
											'type'		=>	'error'
										] ;
			}

		echo json_encode($response);
	}

	public function downlineWithdrawal(){

		if($this->is_logged_in() ) {	

						$data = [
									'withdrawals' => [],
									'datefrom'	  => 'No date set',
									'dateto'	  => 'No date set',	
									'userid'	 => '<i>(NO USER SELECTED)</i>'

								] ;
					return $this->load->view('withdrawal/search_downline_withdrawal',$data);
				}
				else {

					redirect('login');
				}

	}
	public function searchWithdrawal() {

		if($this->is_logged_in() ) {	


					$userid = $this->input->post('userid');
					$df 	= $this->input->post('df');
					$dt 	= $this->input->post('dt');
					$tt 	= $this->input->post('tt');

					$result = [] ;


					foreach ($this->model_matrix_downline_ref->select('down_id',['income_id'=>$userid]) as $key => $value) {
							
							if($tt == "all") {

								foreach ($this->model_withdraw_request->query("SELECT * from withdraw_request where user_id='".$value->down_id."' AND (posted_date BETWEEN '".$df."' AND '".$dt."') AND status=0  ")->result() as $key => $value1) {
									
									array_push($result,[
														'user_id'		=>	$value1->user_id,
		                            					'acc_name'		=>	$value1->acc_name,
		                            					'acc_number'	=>	$value1->acc_number,
		                            					'bank_nm'		=>	$value1->bank_nm,
		                            					'branch_nm'		=>	$value1->branch_nm,
		                            					'swift_code'	=>	$value1->swift_code,
		                            					'request_amount'=>	$value1->request_amount,
		                            					'transaction_no' => $value1->transaction_number,
		                            					'admin_remark' => $value1->admin_remark
													]);
								}
							}

							else {

									foreach ($this->model_withdraw_request->query("SELECT * from withdraw_request where  user_id='".$value->down_id."'  AND  posted_date BETWEEN '".$df."' AND '".$dt."' AND trans_type='".$tt."' AND status=0")->result() as $key => $value1) {

										array_push($result,[
															'user_id'		=>	$value1->user_id,
			                            					'acc_name'		=>	$value1->acc_name,
			                            					'acc_number'	=>	$value1->acc_number,
			                            					'bank_nm'		=>	$value1->bank_nm,
			                            					'branch_nm'		=>	$value1->branch_nm,
			                            					'swift_code'	=>	$value1->swift_code,
			                            					'request_amount'=>	$value1->request_amount,
			                            					'transaction_no' => $value1->transaction_number,
			                            					'admin_remark' => $value1->admin_remark
													]);	
								}
							}
						}

						$data = [
									'withdrawals' => $result,
									'datefrom'	  => $df,
									'dateto'	  => $dt,	
									'userid'	  => $userid
								] ;

					return $this->load->view('withdrawal/search_downline_withdrawal',$data);
				}
				else {

					redirect('login');
				}

	}
	public function pay(){

		$user_id 		 = $this->input->post('user_id');
		$transaction_no  = $this->input->post('transaction_no');
		$amount 		 = $this->input->post('amount');
		$date 			 = date('Y-m-d');

		$response 		= [] ;
		$ewallet 		= 0 ;
		$finalbalance   = 0 ;
 	


		$data 	= [] ;


			$result  = $this->model_withdraw_request->update(['status'=>'1','admin_response_date'=>$date,'admin_remark'=>'PAID'],['user_id'=>$user_id,'transaction_number'=>$transaction_no]);

		if($result) {
							$response  = [

											'title' 	=>	'Payment Sucess',
											'text'		=> 	'Request has  been paid',
											'type'		=>	'success'
										] ;
			}
			else {
							$response = [
											'title' 	=>	'Payment Failed',
											'text'		=> 	'Something went wrong',
											'type'		=>	'error'
										] ;
			}

		echo json_encode($response);
	}

	public function managecoins(){

		if($this->is_logged_in() ) {	
					return $this->load->view('withdrawal/search_downline_withdrawal');
					
				}
		else {
					redirect('login');
		}

	} 
	public function managwithdrawals(){
		if( $this->is_logged_in() ) {	

				return $this->load->view('withdrawal/manage');
					
			}
		else {

			redirect('login');
		}
	}

	public function updateStatus() {

			$response = [] ;


			$status = $this->input->post('status');

			if($this->model_users->query("Update popup set status='".$status."',date='".date('Y-m-d')."' where id=2")) {

						$response  = [
										'title' 	=>	'Update Sucess',
										'text'		=> 	'Status has been updated',
										'type'		=>	'success'
									] ;
				}
				else {

							$response  = [
											'title' 	=>	'Update Error',
											'text'		=> 	'Status has been failed to update',
											'type'		=>	'error'
										] ;
				}
			
		  echo json_encode($response);

	}
}