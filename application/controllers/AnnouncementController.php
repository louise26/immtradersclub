<?php
defined('BASEPATH') or exit('No direct script access allowed');



class AnnouncementController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		

	}

	public function index () {
			
				// if($this->is_logged_in() ) {

				// 	$data = [
				// 				'announcement'	=> $this->model_users->query("SELECT * from promo")->result()
				// 			];
						
				// 	return $this->load->view('member_management/announcementView',$data);
				// }
				// else {

				// 	redirect('login');
				// }
				
				$updateWallet = [];
		foreach ($this->model_users->select('user_id',['user_rank_name <>' =>'Normal User']) as $key => $value) {
		    
		    $amount = 0;
					foreach ($this->model_credit_amt->query("SELECT sum(credit_amt) as total_credit,sum(debit_amt) as total_debit from credit_debit WHERE id between 556 AND 1556 and ttype <> 'Bankwire'")->result() as $key => $value1) {
				 			
					$amount = ($value1->total_credit) - ($value1->total_debit);


                                
                                if($amount > 0){
                                    
                                    
										        	 			 array_push($updateWallet,[
													        	 							'user_id' => $value->user_id,
													        	 							'amount'  => $amount ,

													        	 						  ]);
                                    
                                }


			 //	echo 'UserID :' .$value->user_id . ' || credited :' . $value1->total_credit . ' || debited : ' .$value1->total_debit . '|| Ewallet balance : ' . $amount. '<br>'	;



			 		
}

				 }
				 
				 $this->updateWallet1($updateWallet1);
				 
				 echo 'ok';
				 
				 
	}
	
	
		public function updateWallet1($data = [] ) {

			$this->db->update_batch('final_e_wallet',$data, 'user_id'); 

	}

	public function updateAnnouncement(){

		if( $this->is_logged_in() ) {

				$result = [] ;


					$this->load->model('model_promo');

					$announcement = [
										'news_name' 	=>	$this->input->post('title'),
										'description'	=> htmlspecialchars($this->input->post('content',FALSE))
									];

					$this->db->truncate('promo');

					if( $this->model_promo->insert( $announcement ) ){

									array_push($result ,[

															'title' 	=>	'Update Sucess',
															'text'		=> 'Announcement has been updated',
															'type'		=>'success'
												 		]) ;


																	
														}
						else {
									array_push($result , [

															'title' 	=>'Update Failed',
															'text'		=> 'Something went wrong',
															'type'		=>'error'
														]);
		

							}


					

				echo json_encode($result);
			
			}

	}
	public function clearAnnouncement() {
				if($this->is_logged_in() ) {


					$result = [] ;


						if($this->db->truncate('promo')){
								array_push($result ,[

														'title' 	=>'Clear Sucess',
														'text'		=> 'Announcement has been cleared',
														'type'		=>'success'
												 	]) ;

						}

						else {
								array_push($result , [

														'title' 	=>'Delete Failed',
														'text'		=> 'Something went wrong',
														'type'		=>'error'
													]);

						}


						echo json_encode($result);
			}


	}
	
	

}