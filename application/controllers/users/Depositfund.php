<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'application/libraries/coinspayment.inc.php';

class  Depositfund  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
		//$this->load->library('CoinPaymentsAPI','coinspayment');


		$this->load->model('model_add_fund_request');
    
        
	}

	
		public function index(){
                


			if($this->is_logged_in()){

					$use_id =$this->auth_user_id;



				$data 		=		[];
				$userinfo 	= 		[];

				$this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,

											'id'			=>  $value->id,

										];
						}

						$data  = [

									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
										

								];
					return $this->load->view('users/depositfund',$data);

			}
			else {

				redirect('login');
			}
		}



		public function deposit() {

				$this->is_logged_in();
				$user_id =$this->auth_user_id;


				$amount 	= $this->input->post('amount');
				$type		= $this->input->post('type');
			
				$username 	= $this->input->post('username');
				$ttype		= 'rwallet';

				$data 		= [] ;

			


				$cps = new CoinPaymentsAPI();
                    
				$cps->Setup('f2637c9d95A745B65CA080F6481788e28CCc10707ED9e0c1bac82D534c75236e', 'e23ad9d213e8bb0966b6a4028461fe7ea9548604a76805cf62acbe8cb320830a');



				if($type =="Bank Transfer"){
								
								//$this->model_credit_amt->query("INSERT INTO add_fund_request values(NULL,'$user_id','$amount','$type','$ttype','Pending','','','".date('Y-m-d')."','','')");
						$this->db->cache_off();
								if($this->model_add_fund_request->insert([
																		'user_id'		=>	$user_id,
																		'amount'		=>	$amount,
																		'paymode'		=> $type,
																		'wallet_type'	=> $ttype,
																		'status'		=> 'Pending',
																		'posted_date'	=>	date('Y-m-d')

																		])) {


											array_push($data , [
														'msg'		=> 'success',
														'txn_id' 	=> '',
														'amount' 	=> $amount,
														'address' 	=> '',
														'img_url' 	=> '',
														'typ' 		=> $type,
														'dest_tag'	=> '',
													]);	

								}
								else {
										array_push($data,[
														'msg'		=> 'Something went wrong',
														'txn_id' 	=> '',
														'amount' 	=> '',
														'address' 	=> '',
														'img_url' 	=> '',
														'typ' 		=> '',
														'dest_tag'	=> '',
													]);	
								}
								
				}

				else {
						
							$result = $cps->CreateTransactionSimple($amount,'USD',$type,'','https://immtradersclub.com/',$username);

							if($result['error'] == 'ok'){

								$rcpt= '<a href="' . $result['result']['status_url'].'" target="_blank">'.$result['result']['txn_id'].'</a>';
								$this->db->cache_off();
								$this->model_add_fund_request->insert([

																		'user_id'				=> $user_id,
																		'amount'				=> $amount,
																		'paymode'				=> $type,
																		'wallet_type'			=> $ttype,
																		'status'				=> 'Waiting for approval',
																		'reciept_no'			=> 	htmlspecialchars($rcpt),
																		'reciept_update_date'	=> 	date('Y-m-d'),
																		'posted_date'			=> 	date('Y-m-d')


																		]);
								

								if($type !="XRP") {
											array_push($data ,[
														'msg'		=> 'success',
														'txn_id' 	=> $result['result']['txn_id'],
														'amount' 	=> $result['result']['amount'],
														'address' 	=> $result['result']['address'],
														'img_url' 	=> $result['result']['qrcode_url'],
														'typ' 		=> $type,
														'dest_tag'	=>  ''
														
													]);	

								}
								else {

										array_push($data ,[
														'msg'		=> 'success',
														'txn_id' 	=> $result['result']['txn_id'],
														'amount' 	=> $result['result']['amount'],
														'address' 	=> $result['result']['address'],
														'img_url' 	=> $result['result']['qrcode_url'],
														'typ' 		=> $type,
														'dest_tag'	=> $result['result']['dest_tag'],
													]);	

								}
								
							}
							else {

									array_push($data,[
														'msg'		=> $result['error'],
														'txn_id' 	=> '',
														'amount' 	=> '',
														'address' 	=> '',
														'img_url' 	=> '',
														'typ' 		=> '',
														'dest_tag'	=> '',
													]);	

							}
				}


					echo  json_encode($data,JSON_UNESCAPED_SLASHES);

		}

}