<?php
defined('BASEPATH') or exit('No direct script access allowed');



class  Immwallet  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_lifejacket_subscription_coin_converted');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_imc_address');

 

	}

	public function index() {
			if($this->is_logged_in()) {
				
				$use_id 	= $this->auth_user_id;
				$data 		=		[];
				$userinfo 	= 		[];
		         $this->db->cache_off();
				foreach ($this->model_users->select('*',['user_id'=>$use_id]) as $key => $value) {

							$userinfo = [
											'user_id' 		=>	$value->user_id,
											'username'		=> 	$value->username,
											'rank'			=> 	$value->user_rank_name,
											'image_name' 	=>  $value->image,
											'fname'			=>  $value->first_name,
											'lname'			=>  $value->last_name,
											'username'		=>  $value->email,
											'id'			=>  $value->id,
											't_code'		=> 	$value->t_code,
										];
						}
						 $this->db->cache_off();
						$data  = [
									'user_id' => $this->auth_user_id,
									'info'	  => $userinfo,
									'rwallet_balance' 	=> $this->model_final_imm_coin_wallet->select('amount',['user_id'=>$use_id]),
									'imc_address'		=>  $this->model_final_imm_coin_wallet->query("Select * from imc_address where user_id='$use_id'")->result()
								];
				return $this->load->view('users/immwallet',$data);
			}
			else {
				redirect('login');
			}


	}


	public function createWallet(){

				$this->is_logged_in();
				$use_id  = $this->auth_user_id;


				$pubkey 	= $this->input->post('publickey');
				$privkey	= $this->input->post('privateKey');
				$mnemonic	= $this->input->post('mnemonic');
				$data = [] ;


			

				$userdata  =  [
									'user_id'		=> $use_id,
									'public_key'	=>$pubkey ,
									'private_key'	=> $privkey,
									'mnemonic'		=> $mnemonic
								];




				if($this->model_imc_address->insert($userdata)){


						$data =  [
												'title'		=> 'Good Job !',
												'msg'		=> 'Wallet Successfully Created',
												'status'	=> 'success'
											];


				}

				else {
							$data =  [
												'title'		=> 'Oops !',
												'msg'		=> 'Something Went Wrong',
												'status'	=> 'error'
											];


				}


			echo json_encode($data);

	}

}