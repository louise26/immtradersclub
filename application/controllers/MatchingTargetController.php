<?php
defined('BASEPATH') or exit('No direct script access allowed');



class MatchingTargetController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');
		$this->load->model('model_matrix_downline_ref');
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_profit_share_all');
		$this->load->model('model_profit_share_exclude');
	}

	public function index () {


					$users = [] ;
					$personal_purchase = 0 ;
					$downline_purchase = 0 ;

					$result = [] ;

				foreach ($this->model_lifejacket_subscription->query("SELECT sum(lifejacket_subscription.amount) as total,lifejacket_subscription.user_id,user_registration.username FROM `lifejacket_subscription` JOIN user_registration on lifejacket_subscription.user_id=user_registration.user_id WHERE lifejacket_subscription.date BETWEEN '2018-01-01' AND '2018-04-30' GROUP BY user_id") ->result() as $key => $value) {

									array_push($result,	[
																				'user_id' => $value->user_id,
																				'username' => $value->username,
																				'personal' => $value->total,
																		]);
							}

							$data = [
										'result' => $result

									];

						return $this->load->view('reports/matchingtarget',$data);
	}

	public function downlines() {



					$result = [] ;

					$user_id = $this->uri->segment(3);
					$amount = $this->uri->segment(4);
					$username = "" ;


					foreach ($this->model_users->select('username',['user_id'=>$user_id]) as $key => $val) {
								$username = $val->username;
					}

					foreach ($this->model_matrix_downline_ref->select('down_id',['income_id' => $user_id,'level' => 1]) as $key => $value1) {
							foreach ($this->model_lifejacket_subscription->query("SELECT sum(lifejacket_subscription.amount) as total,user_registration.user_id,user_registration.username FROM `lifejacket_subscription` JOIN user_registration on lifejacket_subscription.user_id=user_registration.user_id WHERE lifejacket_subscription.date BETWEEN '2018-01-01' AND '2018-04-30' AND lifejacket_subscription.user_id='".$value1->down_id."'") ->result() as $key => $value) {

								$totaldowns = 0 ;
								foreach ($this->model_matrix_downline_ref->select('down_id',['income_id' => $value->user_id]) as $key => $val) {
									foreach ($this->model_lifejacket_subscription->query("SELECT sum(amount) as tamount FROM lifejacket_subscription WHERE user_id='".$val->down_id."'")->result() as $key => $vals) {
										 		$totaldowns = $vals->tamount;
									}
								}
										array_push($result,	[
															'user_id' => $value->user_id,
															'username' => $value->username,
															'personal' => $value->total,
															'down'  => $totaldowns

														]);
							}

						}


								$data = [
											'result' => $result,
											'user_id'	=> $user_id,
											'amount'	=> $amount,
											'username' => $username

									];

						return $this->load->view('reports/downline',$data);



	}
	public function downlineofdownline(){


					$result = [] ;

					$user_id = $this->uri->segment(3);
					$amount = $this->uri->segment(4);
					$username = "" ;


					foreach ($this->model_users->select('username',['user_id'=>$user_id]) as $key => $val) {
								$username = $val->username;
					}

					$totaldowns = 0 ;

					foreach ($this->model_matrix_downline_ref->select('down_id',['income_id' => $user_id]) as $key => $value1) {
							foreach ($this->model_lifejacket_subscription->query("SELECT sum(lifejacket_subscription.amount) as total,user_registration.user_id,user_registration.username FROM `lifejacket_subscription` JOIN user_registration on lifejacket_subscription.user_id=user_registration.user_id WHERE lifejacket_subscription.date BETWEEN '2018-01-01' AND '2018-04-30' AND lifejacket_subscription.user_id='".$value1->down_id."'") ->result() as $key => $value) {

									array_push($result,	[
																				'user_id' => $value->user_id,
																				'username' => $value->username,
																				'personal' => $value->total,
																			]);
							}



						}


								$data = [
											'result' => $result,
											'user_id'	=> $user_id,
											'amount'	=> $amount,
											'username' => $username

									];

						return $this->load->view('reports/downline',$data);



		}


}
