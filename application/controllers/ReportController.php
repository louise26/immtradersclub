<?php
defined('BASEPATH') or exit('No direct script access allowed');



class ReportController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		// Models loaded
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_add_fund_request');
		$this->load->model('model_credit_amt');
		$this->load->model('model_withdraw_request');

	}

	public function index () {
			
				if($this->is_logged_in() ) {	







						$data = [
									'withdrawals' => [],
									'datefrom'	  => 'No date set',
									'dateto'	  => 'No date set',	
									'userid'	 => '<i>(NO USER SELECTED)</i>',
									'ttype'		 => '<i>Search Transaction</i>',
									'total'			=> 0

								] ;
					return $this->load->view('reports/income_reports',$data);
				}
				else {

					redirect('login');
				}
	}

	public function searchIncome(){

		if( $this->is_logged_in() ){

					$userid 	= $this->input->post('userid');
					$df 		= $this->input->post('df');
					$dt 		= $this->input->post('dt');
					$tt 		= $this->input->post('tt');
					$result 	= [] ;
					$condition	= "";
					$total 			= 0 ;

					if($df !="" && $dt !="") {

						$condition = "AND CAST(ts as date) BETWEEN '".$df."' AND '".$dt."' ";

					}

					foreach ($this->model_credit_amt->query("SELECT * FROM credit_debit  WHERE user_id='".$userid."' AND ttype='".$tt."' $condition")->result() as $key => $value) {

							foreach ($this->model_users->select('username,first_name,last_name',['user_id'=>$value->user_id]) as $key => $value1) {
								

									array_push($result,[
													'user_id'	=> $value->user_id,
													'username'	=> $value1->username,
													'fullname'	=> $value1->first_name . " " . $value1->last_name,
													'commission' => $value->credit_amt,
													'ttype'		=> $value->ttype,
													'remark'	=> $value->TranDescription,
													'status'	=> 'Paid',
													'ts'		=> $value->ts
										]);

								}
								$total = $total + $value->credit_amt;
					}

						

						$data = [
									'withdrawals' => $result,
									'datefrom'	  => $dt,
									'dateto'	  => $df,	
									'userid'	 => $userid,
									'ttype'		 => $tt ,
									'total'			=> $total

								];


				return $this->load->view('reports/income_reports',$data);
		}
		else {
			redirect('login');
		}
	}

}