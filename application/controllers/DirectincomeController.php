<?php
defined('BASEPATH') or exit('No direct script access allowed');



class DirectincomeController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		
		
	}

	public function index () {
			
				if($this->is_logged_in() ) {

						$data = [
									'withdrawals' => [],
									'datefrom'	  => 'No date set',
									'dateto'	  => 'No date set',	
									'userid'	 => '<i>(NO USER SELECTED)</i>',
									'ttype'		 => '<i>Search Transaction</i>',
									'total'			=> 0

								] ;
					
						
					return $this->load->view('reports/directincome',$data);
				}
				else {

					redirect('login');
				}
	}


	public function searchIncome(){

		if( $this->is_logged_in() ){

					$userid 	= $this->input->post('userid');
					$df 		= $this->input->post('df');
					$dt 		= $this->input->post('dt');
					$tt 		= $this->input->post('tt');
					$result 	= [] ;
					$condition	= "";
					$total 			= 0 ;

					if($df !="" && $dt !="") {

						$condition = "AND CAST(ts as date) BETWEEN '".$df."' AND '".$dt."' ";

					}

					foreach ($this->model_credit_amt->query("SELECT * FROM matrix_downline_ref  WHERE income_id='".$userid."' ")->result() as $key => $value) {

							foreach ($this->model_users->select('username,first_name,last_name,registration_date,user_status',['user_id'=>$value->down_id]) as $key => $value1) {
								

									array_push($result,[
													'user_id'	=> $value->down_id,
													'username'	=> $value1->username,
													'fullname'	=> $value1->first_name . " " . $value1->last_name,
													'registratio_date' => date('F d,Y',strtotime($value1->registration_date)),
													'status'	=> $value1->user_status,
													
										]);

								}
							
					}

						

						$data = [
									'withdrawals' => $result,
									'datefrom'	  => $dt,
									'dateto'	  => $df,	
									'userid'	 => $userid,
									
									'total'			=> $total

								];


				return $this->load->view('reports/directincome',$data);
		}
		else {
			redirect('login');
		}
	}


}
