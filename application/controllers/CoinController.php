<?php
defined('BASEPATH') or exit('No direct script access allowed');



class CoinController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		// Models loaded
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_add_fund_request');
		$this->load->model('model_credit_amt');
		$this->load->model('model_withdraw_request');

	}

	public function index () {
			
				if($this->is_logged_in() ) {	
					return $this->load->view('coinsection/history');
				}
				else {

					redirect('login');
				}
	}


	public function getList(){

			if($this->is_logged_in() ) {	
					
						$i = 0;
						$data = [];
						foreach ($this->model_users->query("SELECT * FROM lifejacket_subscription_coin JOIN user_registration ON lifejacket_subscription_coin.user_id=user_registration.user_id order by lifejacket_subscription_coin.id desc ")->result() as $key => $value) {
							$row 	= array();
							$i 		+= 1;

							array_push($data,
											[
												$i,
												$value->transaction_no,
												$value->user_id, 
												$value->username ,
												$value->date,
												$value->amount,
												$value->pb,
												$value->package,
												$value->sponsor,
												'Paid',

											]);
				}


			
			
					$output = array(
							"data" => $data,
						);  
	 			echo json_encode($output,JSON_UNESCAPED_SLASHES);
			
				}
				else {

					redirect('login');
				}


	}

}
