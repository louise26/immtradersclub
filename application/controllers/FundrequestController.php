<?php
defined('BASEPATH') or exit('No direct script access allowed');



class FundrequestController  extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		// Force SSL
		//$this->force_ssl();
		// Form and URL helpers always loaded (just for convenience)
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->model('model_users');	
		$this->load->model('model_matrix_downline_ref');	
		$this->load->model('model_final_e_wallet');
		$this->load->model('model_final_reg_wallet');
		$this->load->model('model_lifejacket_subscription');
		$this->load->model('model_credit_amt');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ethereum_wallet');
		$this->load->model('model_final_ethereum_classic_wallet');
		$this->load->model('model_final_imm_coin_wallet');
		$this->load->model('model_final_ripple_wallet');
		$this->load->model('model_final_bitcoin_wallet');
		$this->load->model('model_acc_close_request');
		$this->load->model('model_add_fund_request');
		

	}

	public function index () {
			
				if($this->is_logged_in() ) {

					
						
					return $this->load->view('member_management/fundrequestView');
				}
				else {

					redirect('login');
				}
	}



	public function getRequest(){


			$data 		 = array();
			$status 	 = "" ;
			$i 			 = 0 ;
			$user_status = "" ;
			$approval = "" ;
			$pending  = "" ;

			if( $this->is_logged_in() ){

				foreach ($this->model_users->query("Select  add_fund_request.id as id,add_fund_request.posted_date,add_fund_request.user_id,add_fund_request.reciept_no,add_fund_request.reciept_update_date,add_fund_request.paymode,add_fund_request.amount,add_fund_request.status,add_fund_request.wallet_type,user_registration.email from add_fund_request  JOIN user_registration ON add_fund_request.user_id=BINARY user_registration.user_id where add_fund_request.status='Waiting for approval' OR add_fund_request.status='Pending'")->result() as $key => $value) {

							$row 	= array();
							$i 		+= 1;

					array_push($data,

									[
										$i,
										$value->user_id, 
										$value->email,
										htmlspecialchars_decode($value->reciept_no),
										date('F d, Y',strtotime($value->posted_date)),
										$value->paymode,
										$value->amount,
										$value->status,
										'<button  class="btn btn-info btn-xs" id="paid" data-wallet="'.$value->wallet_type.'" data-amount="'.$value->amount.'" data-id="'.$value->user_id.'" value="'.$value->id.'"><i class="fa fa-check"></i> Mark as Paid </button>
										<button  class="btn btn-warning btn-xs" id="cancel" data-wallet="'.$value->wallet_type.'" data-amount="'.$value->amount.'" data-id="'.$value->user_id.'" value="'.$value->id.'"><i class="fa fa-times"></i> Cancel </button>'
									]);

						
				}
			}
			else {

					$data[] = [''] ;

			}

		$output = array(
							"data" => $data,
						);  
			       


	 	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}


	


		public function getClose(){


			$data 		 = array();
			$status 	 = "" ;
			$i 			 = 0 ;
			$user_status = "" ;

			if( $this->is_logged_in() ){

					foreach ($this->model_users->query("Select  add_fund_request.user_id,add_fund_request.reciept_no,add_fund_request.wallet_type,add_fund_request.reciept_update_date,add_fund_request.paymode,add_fund_request.amount,add_fund_request.status,user_registration.first_name,user_registration.last_name from add_fund_request  JOIN user_registration ON add_fund_request.user_id=BINARY user_registration.user_id where add_fund_request.status='Paid' OR add_fund_request.status ='Cancel'OR add_fund_request.status='Cancelled'")->result() as $key => $value) {

							$row 	= array();
							$i 		+= 1;

					array_push($data,

									[
										$i,
										$value->user_id, 
										$value->first_name . ' ' .$value->last_name,
										htmlspecialchars_decode($value->reciept_no),
										date('F d, Y',strtotime($value->reciept_update_date)),
										$value->paymode,
										$value->wallet_type,
										$value->amount,
										$value->status
									]);

						
				}



			}
			else {

					$data[] = [''] ;

			}

		$output = array(
							"data" => $data,
						);  
			       


	 	echo json_encode($output,JSON_UNESCAPED_SLASHES);
	}



	public function approveRequest() {
		
		if( $this->is_logged_in() ){

								$result 		= [] ;
								$id 	 		= $this->input->post('id');
								$user_id 		= $this->input->post('user_id');
								$wallet_type 	= $this->input->post('wallet_type');
								$amount 		= $this->input->post('amount');


								$wallet = 0 ;
								$newwalletbalance = 0;
								$rand=$user_id.rand(00001,99999);
					       	    $urls="http://".$_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

					       	    		



					       	    		
							if( $wallet_type=='rwallet' ) {
											$this->db->cache_off();

													foreach ($this->model_final_reg_wallet->select('amount',['user_id'=>$user_id]) as $key => $value) {
					       	    							
					       	    						$wallet = $value->amount;

					       	    					}
					       	   					 $newwalletbalance = $wallet + $amount;
					                			
					                				$this->model_final_reg_wallet->update(['amount'=>$newwalletbalance],['user_id'=>$user_id]);
					                   				$this->model_credit_amt->insert([

					                   													'transaction_no' => $rand,
					                   													'user_id'		 => $user_id,
					                   													'credit_amt'	 => $amount,
					                   													'debit_amt'		 => 0,
					                   													'admin_charge'	 => 0,
					                   													'receiver_id'	 => $user_id,
					                   													'sender_id'		 => '123456',
					                   													'receive_date'   => date('Y-m-d'),
					                   													'ttype'			 => 'Add fund request',
					                   													'TranDescription'=> 'Add fund request approved by admin',
					                   													'Cause'			 => 'Fund Credited By Admin',
					                   													'Remark'		 => 'Fund Credited By Admin',
					                   													'invoice_no'	 => $rand,
					                   													'product_name'   => 'Fund Credited',
					                   													'status'		 => 0,
					                   													'ewallet_used_by' => 'Register Wallet',
					                   													'current_url'	 => $urls,

					                   												]);
					                   				$this->model_add_fund_request->update(['status' => 'Paid', 'admin_approval_date'=> date('Y-m-d')],['id'=>$id]);

					                   					$result  = [
																	 'title' 	=>	'Paid Sucess',
																	 'text'		=> 	'Fund has been paid',
																	 'type'		=>	'success'
																	] ;

					             
							}
							else
							{
								
												$this->db->cache_off();
													foreach ($this->model_final_e_wallet->select('amount',['user_id'=>$user_id]) as $key => $value) {
					       	    							
					       	    						$wallet = $value->amount;

					       	    					}
					       	   					 $newwalletbalance = $wallet + $amount;
					                			
					                				$this->model_final_e_wallet->update(['amount'=>$newwalletbalance],['user_id'=>$user_id]);

						                   				$this->model_credit_amt->insert([

						                   													'transaction_no' => $rand,
						                   													'user_id'		 => $user_id,
						                   													'credit_amt'	 => $amount,
						                   													'debit_amt'		 => 0,
						                   													'admin_charge'	 => 0,
						                   													'receiver_id'	 => $user_id,
						                   													'sender_id'		 => '123456',
						                   													'receive_date'   => date('Y-m-d'),
						                   													'ttype'			 => 'Add fund request',
						                   													'TranDescription'=> 'Add fund request approved by admin',
						                   													'Cause'			 => 'Fund Credited By Admin',
						                   													'Remark'		 => 'Fund Credited By Admin',
						                   													'invoice_no'	 => $rand,
						                   													'product_name'   => 'Fund Credited',
						                   													'status'		 => 0,
						                   													'ewallet_used_by' => 'Withdrawal Wallet',
						                   													'current_url'	 => $urls,
						                   												]);

						                   $this->model_add_fund_request->update(['status' => 'Paid', 'admin_approval_date'=> date('Y-m-d')],['id'=>$id]);

						                   					$result  = [

																		'title' 	=>	'Paid Sucess',
																		'text'		=> 	'Fund has been paid',
																		'type'		=>	'success'
																	] ;


								}

				echo json_encode($result);
			
		}

	}

	public function cancelRequest() {
		
		if($this->is_logged_in()){

			$result = [] ;
			$user_id 		= $this->input->post('id');
			$id 			= $this->input->post('user_id');
			$wallet_type	= $this->input->post('wallet_type');
			$amount 		= $this->input->post('amount');


		
			//update add_fund_request set status='$status', admin_approval_date='".date('Y-m-d')."' where id='$id' and user_id='$user'
			$result  = $this->model_add_fund_request->update([
																'status' 				=> 'Cancelled',
																'admin_approval_date'	=> date('Y-m-d')

															 ],
															 [
																	'id'=>$user_id
															 ]);

		
			if($result) {
						$result  = [

															'title' 	=>	'Cancel Sucess',
															'text'		=> 	'Request has  been cancelled',
															'type'		=>	'success'
												 		] ;
			}
			else {

						$result = [

															'title' 	=>	'Cancel Failed',
															'text'		=> 	'Something went wrong',
															'type'		=>	'error'
												 		]  ;
			}

			echo json_encode($result);
			

		}


		
	}


}
